﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

public class TouchAndDragController : MonoBehaviour
{

    public GameObject objectToPlacePrefab;
    public Camera firstPersonCamera;
    public Text debugText;

    private GameObject placedObject;
    private bool isDragging = false;
    private Vector3 touchOffset;

    void Update()
    {

        Touch touch = Input.GetTouch(0);
        if (Input.touchCount < 1)
        {
            isDragging = false;
            return;
        }

        if (touch.phase == TouchPhase.Began)
        {

            // Raycast against GameObjects
            Ray ray = firstPersonCamera.ScreenPointToRay(touch.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity,
                Physics.AllLayers, QueryTriggerInteraction.Collide))
            {
                if (hit.transform.gameObject.tag == "Movible")
                {
                    debugText.text = "Objeto movible seleccionado";
                    isDragging = true;
                    GameObject touchObject = hit.transform.gameObject;
                    float xOffset =
                        touchObject.transform.position.x - hit.point.x;
                    float zOffset =
                        touchObject.transform.position.z - hit.point.z;
                    touchOffset = new Vector3(xOffset,
                        touchObject.transform.position.y,
                        zOffset);
                }
            }
        }
        
        TrackableHit trackableHit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
            TrackableHitFlags.FeaturePointWithSurfaceNormal;

        // Raycast against ARCore trackables
        if (Frame.Raycast(touch.position.x,
            touch.position.y, raycastFilter, out trackableHit))
        {
            if (placedObject == null)
            {
                placedObject = Instantiate(objectToPlacePrefab,
                    trackableHit.Pose.position,
                    trackableHit.Pose.rotation);
                var anchor =
                    trackableHit.Trackable.CreateAnchor(trackableHit.Pose);
                placedObject.transform.parent = anchor.transform;
                debugText.text = "Objeto colocado";
            }
            else if (isDragging)
            {
                debugText.text = "Objeto movido";
                Vector3 dragPos =
                    new Vector3(trackableHit.Pose.
                        position.x - touchOffset.x,
                        trackableHit.Pose.position.y,
                        trackableHit.Pose.position.z - touchOffset.z);
                placedObject.transform.position = dragPos;
                Vector3 planarY = firstPersonCamera.transform.position;
                planarY.y = trackableHit.Pose.position.y;
                placedObject.transform.LookAt(planarY,
                    placedObject.transform.up);
            }
        }
    }
}