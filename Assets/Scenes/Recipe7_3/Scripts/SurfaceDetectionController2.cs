﻿using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using UnityEngine.UI;
using GoogleARCore.Examples.Common;

public class SurfaceDetectionController2 : MonoBehaviour
{

    public Camera FirstPersonCamera;
    public GameObject TrackedPlanePrefab;

    private List<DetectedPlane> newPlanes = new List<DetectedPlane>();

    public void Update()
    {
        Session.GetTrackables<DetectedPlane>(newPlanes, TrackableQueryFilter.New);
        for (int i = 0; i < newPlanes.Count; i++)
        {
            GameObject planeObject = Instantiate(TrackedPlanePrefab, Vector3.zero, Quaternion.identity, transform);
            planeObject.GetComponent<DetectedPlaneVisualizer>().Initialize(newPlanes[i]);
        }
 
    }
}