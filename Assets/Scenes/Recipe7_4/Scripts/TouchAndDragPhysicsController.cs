﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using UnityEngine.UI;

public class TouchAndDragPhysicsController : MonoBehaviour
{

    public GameObject objectToPlacePrefab;
    public Camera firstPersonCamera;
    public Text snackBarText;
    public SpringJoint springJoint;
    public GameObject invisibleFloorPrefab;
    public DragLineController dragLineController;

    private GameObject placedObject;
    private float grabbedDistance;
    private Vector3 localHitPosition;
    private Ray dragRay;

    void Start()
    {
        dragLineController.gameObject.transform.SetParent(firstPersonCamera.transform);
        dragLineController.lineRenderer.enabled = false;
    }

    void Update()
    {
        Touch touch = Input.GetTouch(0);
        if (Input.touchCount < 1)
        {
            if (springJoint.connectedBody)
            {
                snackBarText.text = "No Touch";
                springJoint.connectedBody = null;
                dragLineController.lineRenderer.enabled = false;
            }
            return;
        }

        Ray touchRay = firstPersonCamera.ScreenPointToRay(touch.position);

        // RAYCASTS AGAINST GRABBABLE OBJECTS IN SCENE
        if (touch.phase == TouchPhase.Began)
        {
            RaycastHit hit;
            if (Physics.Raycast(touchRay, out hit, Mathf.Infinity,
                Physics.AllLayers, QueryTriggerInteraction.Collide))
            {
                if (hit.transform.gameObject.tag == "Movible")
                {
                    //snackBarText.text = "Objeto movible seleccionado";
                    if (!springJoint.connectedBody)
                    {
                        snackBarText.text = "SpringJoint creado";
                        Rigidbody rigidbody = hit.rigidbody;
                        if (rigidbody)
                        {
                            springJoint.connectedBody = rigidbody;
                            localHitPosition = hit.point;
                            localHitPosition = springJoint.connectedBody.transform.InverseTransformPoint(localHitPosition);
                            Vector3 grabPos = Vector3.Lerp(springJoint.connectedBody.transform.position, hit.point, 0.5f);
                            springJoint.transform.position = grabPos;
                            springJoint.connectedAnchor = springJoint.connectedBody.transform.InverseTransformPoint(grabPos);
                            grabbedDistance = hit.distance;
                            dragLineController.lineRenderer.enabled = true;
                        }
                    }
                }
            }
            dragRay = touchRay;
        }
        else if (touch.phase == TouchPhase.Moved
            || touch.phase == TouchPhase.Stationary)
        {
            snackBarText.text = "Movido";
            dragRay = touchRay;
        }
        else if (touch.phase == TouchPhase.Ended)
        {
            snackBarText.text = "Desplazamiento finalizado";
            springJoint.connectedBody = null;
            dragLineController.lineRenderer.enabled = false;
        }

        /// RAYCASTS AGAINST ARCORE SURFACES BEING TRACKED
        TrackableHit trackableHit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
            TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (Frame.Raycast(touch.position.x,
            touch.position.y, raycastFilter, out trackableHit))
        {
            if (placedObject == null)
            {

                placedObject = Instantiate(objectToPlacePrefab, trackableHit.Pose.position, objectToPlacePrefab.transform.rotation);

                var anchor = trackableHit.Trackable.CreateAnchor(trackableHit.Pose);

                placedObject.transform.parent = anchor.transform;

                // create the physics floor for the object to bounce off
                GameObject invisibleFloor = Instantiate(invisibleFloorPrefab, trackableHit.Pose.position, invisibleFloorPrefab.transform.rotation);

                invisibleFloor.transform.parent = anchor.transform;
            }
        }
    }

    private void FixedUpdate()
    {
        if (springJoint.connectedBody)
        {
            snackBarText.text = "Fuerzas modificadas";
            springJoint.transform.position = dragRay.origin + dragRay.direction * grabbedDistance;
            springJoint.connectedBody.AddForce(-Physics.gravity * 0.75f, ForceMode.Acceleration);
            springJoint.connectedBody.AddForce(-springJoint.connectedBody.velocity * 0.5f, ForceMode.Acceleration);
            Vector3 angularVelocity = springJoint.connectedBody.angularVelocity;
            angularVelocity /= 1.0f + 1.0f * Time.deltaTime;
            springJoint.connectedBody.angularVelocity = angularVelocity;
        }
    }

    private void LateUpdate()
    {
        if (dragLineController.lineRenderer.enabled)
        {
            dragLineController.startPosition =
                dragLineController.transform.position;
            dragLineController.endPosition =
                springJoint.connectedBody.transform
                    .TransformPoint(localHitPosition);

            float lineDistance =
                Vector3.Distance(dragLineController.startPosition, dragLineController.endPosition);
            dragLineController.controlPosition = dragLineController.transform.position + dragLineController.transform.forward * (lineDistance / 2);
            dragLineController.UpdateLineRenderer();
        }
    }
}