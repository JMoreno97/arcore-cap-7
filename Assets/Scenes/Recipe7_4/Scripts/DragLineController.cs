﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragLineController : MonoBehaviour
{

    [System.NonSerialized]
    public LineRenderer lineRenderer;
    [System.NonSerialized]
    public Vector3 startPosition;
    [System.NonSerialized]
    public Vector3 controlPosition;
    [System.NonSerialized]
    public Vector3 endPosition;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void UpdateLineRenderer()
    {
        if (lineRenderer.enabled)
        {
            for (int i = 0; i < lineRenderer.positionCount; i++)
            {
                float ratio = (float)i / (lineRenderer.positionCount - 1);
                Vector3 pos =
                  Bezier(startPosition, controlPosition, endPosition, ratio);
                lineRenderer.SetPosition(i, pos);
            }
        }
    }

    private Vector3 Bezier(Vector3 a, Vector3 b, Vector3 c, float ratio)
    {
        Vector3 startToMid = Vector3.Lerp(a, b, ratio);
        Vector3 midToEnd = Vector3.Lerp(b, c, ratio);
        return Vector3.Lerp(startToMid, midToEnd, ratio);
    }
}