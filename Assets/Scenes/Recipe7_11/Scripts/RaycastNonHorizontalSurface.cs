﻿using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;
public class RaycastNonHorizontalSurface : MonoBehaviour
{
    public GameObject objectToPlacePrefab;
    public Camera firstPersonCamera;
    public Text snackBarText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Touch touch;
        if (Input.touchCount < 1 ||
            (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
        {
            return;
        }
        TrackableHit trackableHit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.FeaturePoint | TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter,
            out trackableHit))
        {
            snackBarText.text = "HIT";
            Anchor anchor = trackableHit.Trackable.CreateAnchor(trackableHit.Pose);
            GameObject placedObject = Instantiate(objectToPlacePrefab,trackableHit.Pose.position, objectToPlacePrefab.transform.rotation);
            placedObject.transform.parent = anchor.transform;
        }
        else
        {
            snackBarText.text = "NO HIT";
        }
    }
}
