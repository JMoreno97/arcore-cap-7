﻿using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using UnityEngine.UI;
using GoogleARCore.Examples.Common;

public class SurfaceDetectionController : MonoBehaviour
{

    public Camera FirstPersonCamera;
    public GameObject TrackedPlanePrefab;
    public Text debugText;
    public GameObject objectToPlacePrefab;
    private int numPlanos = 0;

    private List<DetectedPlane> newPlanes = new List<DetectedPlane>();

    public void Update()
    {
        Session.GetTrackables<DetectedPlane>(newPlanes, TrackableQueryFilter.New);
        for (int i = 0; i < newPlanes.Count; i++)
        {
            numPlanos++;
            debugText.text = "Nuevo plano encontrado. LLevo: "+numPlanos;
            GameObject planeObject = Instantiate(TrackedPlanePrefab, Vector3.zero, Quaternion.identity,transform);
            planeObject.GetComponent<DetectedPlaneVisualizer>()
                .Initialize(newPlanes[i]);
        }

        Touch touch;
        if (Input.touchCount < 1 ||
            (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
        {
            return;
        }

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
            TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (Frame.Raycast(touch.position.x,
            touch.position.y,
            raycastFilter,
            out hit))
        {
            GameObject placedObject =
                Instantiate(objectToPlacePrefab,
                    hit.Pose.position,
                    hit.Pose.rotation);

            var anchor = hit.Trackable.CreateAnchor(hit.Pose);

            if ((hit.Flags &
                TrackableHitFlags.PlaneWithinPolygon) !=
                TrackableHitFlags.None)
            {
                Vector3 cameraPositionSameY =
                    FirstPersonCamera.transform.position;
                cameraPositionSameY.y = hit.Pose.position.y;
                placedObject.transform.LookAt(cameraPositionSameY,
                    placedObject.transform.up);
            }
            placedObject.transform.parent = anchor.transform;
        }
    }
}