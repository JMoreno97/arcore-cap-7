﻿using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

public class TouchAndRotateController : MonoBehaviour
{

    public GameObject objectToPlacePrefab;
    public Camera firstPersonCamera;
    public Text snackBarText;

    private GameObject touchObject;
    private bool isRotating = false;
    private Vector2 previousPos;

    void Update()
    {

        Touch touch = Input.GetTouch(0);
        if (Input.touchCount < 1)
        {
            return;
        }

        // RAYCASTS AGAINST GRABBABLE OBJECTS IN SCENE
        if (touch.phase == TouchPhase.Began)
        {
            Ray ray = firstPersonCamera.ScreenPointToRay(touch.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity,
                Physics.AllLayers, QueryTriggerInteraction.Collide))
            {
                if (hit.transform.gameObject.tag == "Movible")
                {
                    isRotating = true;
                    touchObject = hit.transform.gameObject;

                    Vector3 bumpUp = new Vector3(touchObject.transform.position.x,touchObject.transform.position.y + (touchObject.GetComponent<Collider>().bounds.size.y / 4f), touchObject.transform.position.z);
                    touchObject.transform.position = bumpUp;
                    previousPos = touch.position;
                }
            }
        }
        else if (touch.phase == TouchPhase.Ended)
        {
            isRotating = false;
            snackBarText.text = "Rotación terminada";
            Transform parentTrans = touchObject.transform.parent;
            Vector3 bumpDown = new Vector3(touchObject.transform.position.x, parentTrans.position.y, touchObject.transform.position.z);
            touchObject.transform.position = bumpDown;
            return;
        }

        /// RAYCASTS AGAINST ARCORE SURFACES BEING TRACKED
        TrackableHit trackableHit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
            TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out trackableHit))
        {
            if (touchObject == null)
            {
                touchObject = Instantiate(objectToPlacePrefab, trackableHit.Pose.position, objectToPlacePrefab.transform.rotation);
                var anchor =
                    trackableHit.Trackable.CreateAnchor(trackableHit.Pose);
                touchObject.transform.parent = anchor.transform;
            }
        }

        if (isRotating)
        {
            snackBarText.text = "Rotando";
            float rot = previousPos.x - touch.position.x;
            touchObject.transform.Rotate(Vector3.up * rot, Space.World);
            previousPos = touch.position;
        }
    }
}