﻿using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

public class TargetController : MonoBehaviour
{

    public GameObject target;
    public TouchAndDragTargetController touchAndDrag;
    public Text snackBarText;

    void Start()
    {
        target.SetActive(false);
    }

    void Update()
    {
        if (touchAndDrag.isDragging)
        {
            target.SetActive(true);
            Transform placedObjTrans = touchAndDrag.placedObject.transform;
            Transform parentTrans = placedObjTrans.parent;
            Vector3 targetPos =
                new Vector3(placedObjTrans.position.x,
                    parentTrans.position.y,
                    placedObjTrans.position.z);
            target.transform.position = targetPos;
        }
        else
        {
            target.SetActive(false);
        }
    }
}