﻿using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

public class TouchAndDragTargetController : MonoBehaviour
{

    public GameObject objectToPlacePrefab;
    public Camera firstPersonCamera;
    public Text snackBarText;
    public bool isDragging = false;
    public GameObject placedObject;

    private Vector3 touchOffset;

    void Update()
    {

        Touch touch = Input.GetTouch(0);
        if (Input.touchCount < 1)
        {
            return;
        }

        // RAYCASTS AGAINST GRABBABLE OBJECTS IN SCENE
        if (touch.phase == TouchPhase.Began)
        {
            Ray ray = firstPersonCamera.ScreenPointToRay(touch.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity,
                Physics.AllLayers, QueryTriggerInteraction.Collide))
            {
                if (hit.transform.gameObject.tag == "Movible")
                {
                    snackBarText.text = "Tap inicial";
                    isDragging = true;
                    GameObject touchObject = hit.transform.gameObject;
                    float xOffset = touchObject.transform.position.x - hit.point.x;
                    float zOffset = touchObject.transform.position.z - hit.point.z;
                    touchOffset = new Vector3(xOffset, touchObject.transform.position.y, zOffset);
                }
            }
        }
        else if (touch.phase == TouchPhase.Ended)
        {
            isDragging = false;
            snackBarText.text = "Objeto movido";
            Transform parentTrans = placedObject.transform.parent;
            Vector3 endPos = new Vector3(placedObject.transform.position.x, parentTrans.position.y, placedObject.transform.position.z);
            placedObject.transform.position = endPos;
            return;
        }

        /// RAYCASTS AGAINST ARCORE SURFACES BEING TRACKED
        TrackableHit trackableHit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
            TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (Frame.Raycast(touch.position.x, touch.
            position.y, raycastFilter, out trackableHit))
        {
            if (placedObject == null)
            {
                placedObject = Instantiate(objectToPlacePrefab,trackableHit.Pose.position, objectToPlacePrefab.transform.rotation);
                var anchor = trackableHit.Trackable.CreateAnchor(trackableHit.Pose);
                placedObject.transform.parent = anchor.transform;
                snackBarText.text = "Objeto colocado";
            }
            else if (isDragging)
            {
                snackBarText.text = "Moviendo";
                Vector3 dragPos = new Vector3(trackableHit.Pose.position.x - touchOffset.x, touchOffset.y + (placedObject.GetComponent<Collider>().bounds.size.y / 4f), trackableHit.Pose.position.z - touchOffset.z);
                placedObject.transform.position = dragPos;
                Vector3 planarY = firstPersonCamera.transform.position;
                planarY.y = trackableHit.Pose.position.y;
                //placedObject.transform.LookAt(planarY, placedObject.transform.up);
            }
        }
    }
}