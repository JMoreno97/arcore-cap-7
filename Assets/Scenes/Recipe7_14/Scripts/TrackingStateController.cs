﻿using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

public class TrackingStateController : MonoBehaviour
{

    public Text snackBarText;

    void Update()
    {
        CheckSessionStatus();
    }

    private void CheckSessionStatus()
    {
        switch (Session.Status)
        {
            case SessionStatus.ErrorApkNotAvailable:
                snackBarText.text = "ErrorApkNotAvailable";
                break;
            case SessionStatus.ErrorPermissionNotGranted:
                snackBarText.text = "ErrorPermissionNotGranted";
                break;
            case SessionStatus.ErrorSessionConfigurationNotSupported:
                snackBarText.text = "ErrorSessionConfigurationNotSupported";
                break;
            case SessionStatus.FatalError:
                snackBarText.text = "FatalError";
                break;
            case SessionStatus.Initializing:
                snackBarText.text = "Initializing";
                break;
            case SessionStatus.LostTracking:
                snackBarText.text = "LostTracking";
                break;
            case SessionStatus.None:
                snackBarText.text = "None";
                break;
            case SessionStatus.NotTracking:
                snackBarText.text = "NotTracking";
                break;
            case SessionStatus.Tracking:
                snackBarText.text = "Tracking";
                break;
            default:
                snackBarText.text = "Problem getting status";
                break;
        }
    }

}