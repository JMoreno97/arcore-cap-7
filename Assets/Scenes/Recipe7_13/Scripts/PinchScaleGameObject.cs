﻿using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

public class PinchScaleGameObject : MonoBehaviour
{

    public GameObject prefabToPlace;
    public Text debugText;

    private bool hasPlacedObject = false;
    private GameObject placedObject;
    private const float SCALE_SPEED = 0.0005f;
    private const float MAX_SCALE = 0.1f;
    private const float MIN_SCALE = 0.001f;

    void Update()
    {
        if (!hasPlacedObject)
        {
            PlaceObjectToScale();
        }
        else
        {
            PinchToScale();
        }
    }

    private void PlaceObjectToScale()
    {
        Touch touch;
        if (Input.touchCount < 1 ||
            (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
        {
            return;
        }
        TrackableHit trackableHit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon | TrackableHitFlags.FeaturePointWithSurfaceNormal;

        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter,
            out trackableHit))
        {
            Anchor anchor = trackableHit.Trackable.CreateAnchor(trackableHit.Pose);
            placedObject = Instantiate(prefabToPlace, trackableHit.Pose.position, prefabToPlace.transform.rotation);
            placedObject.transform.parent = anchor.transform;
            hasPlacedObject = true;
        }
    }

    private void PinchToScale()
    {
        if (Input.touchCount == 2)
        {
            Touch tZero = Input.GetTouch(0);
            Touch tOne = Input.GetTouch(1);

            Vector2 tZeroPrevPos = tZero.position - tZero.deltaPosition;
            Vector2 tOnePrevPos = tOne.position - tOne.deltaPosition;

            float prevTouchDeltaMag = (tZeroPrevPos - tOnePrevPos).magnitude;
            float tDeltaMag = (tZero.position - tOne.position).magnitude;

            float deltaMagnitudeDiff = tDeltaMag - prevTouchDeltaMag;
            Vector3 scaleDiff = new Vector3(deltaMagnitudeDiff, deltaMagnitudeDiff, deltaMagnitudeDiff);
            Vector3 newScale = placedObject.transform.localScale + scaleDiff * SCALE_SPEED;

            //Vector3 scaleNormalized = Vector3.ClampMagnitude(newScale, MAX_SCALE);
            //placedObject.transform.localScale = new Vector3(Mathf.Abs(scaleNormalized.x), Mathf.Abs(scaleNormalized.y), Mathf.Abs(scaleNormalized.z));
            placedObject.transform.localScale = ClampMagnitude(newScale, MAX_SCALE, MIN_SCALE);
            debugText.text = "x: " + placedObject.transform.localScale.x + ", y: " + placedObject.transform.localScale.y + ", z: " + placedObject.transform.localScale.z;
        }
    }
    private Vector3 ClampMagnitude(Vector3 v, float max, float min)
    {
        if (v.x > (double)max) return new Vector3(max, max, max);
        else if (v.x < (double)min) return new Vector3(min, min, min);
        return v;
    }

}